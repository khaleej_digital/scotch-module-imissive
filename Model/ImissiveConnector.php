<?php
declare(strict_types=1);

namespace Beside\Imissive\Model;

use Beside\Imissive\Api\ImissiveConnectorInterface;
use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\HTTP\Client\CurlFactory;

/**
 * Class ImissiveConnector
 *
 * @package Beside\Imissive\Model
 */
class ImissiveConnector implements ImissiveConnectorInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var CurlFactory
     */
    private CurlFactory $curlFactory;

    /**
     * ElitbuzzConnector constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param CurlFactory $curlFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CurlFactory $curlFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curlFactory = $curlFactory;
    }

    /**
     * Send SMS, return request and response data as array
     *
     * @param string $number
     * @param string $message
     *
     * @return array
     */
    public function sendSms(string $number, string $message): array
    {
        $errorMessage = null;
        $body = null;
        $status = null;
        $url = null;

        /** @var Curl $curl */
        $curl = $this->curlFactory->create() ;
        $headers['Content-Type'] = 'application/json';
        try {
            $url = $this->getApiUrl();
            $params = [
                'user' => $this->getApiUsername(),
                'pass' => $this->getApiPassword(),
                'to' => $number,
                'message' => $message,
                'sender' => $this->getSenderId()
            ];

            $url .= '?' . http_build_query($params);
            $curl->get($url);
            $body = $curl->getBody();
            $status = $curl->getStatus();
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            if (!$errorMessage) {
                $errorMessage = __('An error has occurred while sending Imissive SMS request');
            }
        }
        $result = [
            'url' => $url,
            'request' => $message,
            'response' => $body,
            'status' => $status,
            'errors' => $errorMessage
        ];

        return $result;
    }

    /**
     * Get URL key from configs
     *
     * @return string
     */
    private function getApiUrl(): string
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_API_URL
        );

        return (string) $result;
    }

    /**
     * Get API username from configs
     *
     * @return string
     */
    private function getApiUsername(): string
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_API_USER
        );

        return (string) $result;
    }

    /**
     * Get API password from configs
     *
     * @return string
     */
    private function getApiPassword(): string
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_API_PASSWORD
        );

        return (string) $result;
    }

    /**
     * Get SenderID from configs
     *
     * @return string
     */
    private function getSenderId(): string
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_SENDER_ID
        );

        return (string) $result;
    }
}

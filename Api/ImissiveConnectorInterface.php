<?php
declare(strict_types=1);

namespace Beside\Imissive\Api;

/**
 * Interface ImissiveConnectorInterface
 *
 * @package Beside\Imissive\Api
 */
interface ImissiveConnectorInterface
{
    /** @var string XML path to IMISSIVE SMS API URL config */
    public const XML_PATH_API_URL = 'beside_sms/imissive/api_url';

    /** @var string XML path to IMISSIVE authentication username config */
    public const XML_PATH_API_USER = 'beside_sms/imissive/user';

    /** @var string XML path to IMISSIVE authentication password config */
    public const XML_PATH_API_PASSWORD = 'beside_sms/imissive/password';

    /** @var string XML path to IMISSIVE SMS SenderID config */
    public const XML_PATH_SENDER_ID = 'beside_sms/imissive/sender_id';

    /**
     * Send SMS, return request and response data as array
     *
     * @param string $number
     * @param string $message
     *
     * @return array
     */
    public function sendSms(string $number, string $message): array;
}
